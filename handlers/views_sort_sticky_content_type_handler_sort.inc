<?php

/**
 * @file
 * Definition of views_sort_content_type_custom_handler_sort.
 */

/**
 * Handle sorting by selected content type(s).
 */
class views_sort_sticky_content_type_handler_sort extends views_handler_sort {

  /**
   * Cannot be exposed.
   */
  function can_expose() { return FALSE; }

  /**
   * Node type options form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Load content type options.
    $options = array();
    foreach (node_type_get_types() as $type => $info) {
      $options[$type] = t($info->name);
    }
    asort($options);

    // Add content type checkboxes.
    $form['types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Select content types'),
      '#options' => $options,
      '#default_value' => $this->options['types'],
    );
  }

  /**
   * Default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['types'] = array('default' => array());
    return $options;
  }

  /**
   * Add sort to query
   */
  function query() {
    $this->ensure_my_table();

    if ($options = array_filter($this->options['types'])) {
      $mysql_array_string = '(\'' . implode('\',\'', $options) . '\')';
      $this->query->add_orderby(NULL, $this->table_alias . '.type NOT IN ' . $mysql_array_string, $this->options['order']);
    }
  }

}
