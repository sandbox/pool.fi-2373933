<?php
/**
 * @file
 * Views integration for views_sort_sticky_content_type.
 */

/**
 * Implements hook_views_data_alter().
 */
function views_sort_sticky_content_type_views_data_alter(&$data) {

  // Add a fake column to the field's table.
  $data['node']['sticky_type'] = array(
    'title' => t('Type: Sticky'),
    'help' => t('Display selected content types first.'),
    'sort' => array(
      'handler' => 'views_sort_sticky_content_type_handler_sort',
    ),
  );
}
